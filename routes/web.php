<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Route::get('contactanos', [ContactoController::class, 'index'])->name('contacto');
Route::get('preguntas-frecuentes', [FrequentlyAskedQuestionsController::class, 'index'])->name('preguntas-frecuentes');


Route::get('services', [ServicesController::class, 'index'])->name('servicios');
Route::get('services/details/', [ServicesController::class, 'show'])->name('servicios.detalles');
Route::get('blogs', [BlogController::class, 'index'])->name('blogs');
Route::get('blogs/post', [BlogController::class, 'show'])->name('blogs.post');
Route::get('courses', [CourseController::class, 'index'])->name('courses');


Route::prefix("administrate")->group(function(){
    Route::get('login', [UserController::class, 'login'])->name('admin.login');
});
