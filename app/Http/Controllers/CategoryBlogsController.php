<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreCategoryBlogsRequest;
use App\Http\Requests\UpdateCategoryBlogsRequest;
use App\Models\CategoryBlogs;

class CategoryBlogsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreCategoryBlogsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCategoryBlogsRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CategoryBlogs  $categoryBlogs
     * @return \Illuminate\Http\Response
     */
    public function show(CategoryBlogs $categoryBlogs)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CategoryBlogs  $categoryBlogs
     * @return \Illuminate\Http\Response
     */
    public function edit(CategoryBlogs $categoryBlogs)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateCategoryBlogsRequest  $request
     * @param  \App\Models\CategoryBlogs  $categoryBlogs
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCategoryBlogsRequest $request, CategoryBlogs $categoryBlogs)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CategoryBlogs  $categoryBlogs
     * @return \Illuminate\Http\Response
     */
    public function destroy(CategoryBlogs $categoryBlogs)
    {
        //
    }
}
