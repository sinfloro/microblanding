<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreFrequentlyAskedQuestionsRequest;
use App\Http\Requests\UpdateFrequentlyAskedQuestionsRequest;
use App\Models\FrequentlyAskedQuestions;

class FrequentlyAskedQuestionsController extends Controller
{
    public function index()
    {
        return view('preguntas-frecuentes');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreFrequentlyAskedQuestionsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreFrequentlyAskedQuestionsRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\FrequentlyAskedQuestions  $frequentlyAskedQuestions
     * @return \Illuminate\Http\Response
     */
    public function show(FrequentlyAskedQuestions $frequentlyAskedQuestions)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\FrequentlyAskedQuestions  $frequentlyAskedQuestions
     * @return \Illuminate\Http\Response
     */
    public function edit(FrequentlyAskedQuestions $frequentlyAskedQuestions)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateFrequentlyAskedQuestionsRequest  $request
     * @param  \App\Models\FrequentlyAskedQuestions  $frequentlyAskedQuestions
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateFrequentlyAskedQuestionsRequest $request, FrequentlyAskedQuestions $frequentlyAskedQuestions)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\FrequentlyAskedQuestions  $frequentlyAskedQuestions
     * @return \Illuminate\Http\Response
     */
    public function destroy(FrequentlyAskedQuestions $frequentlyAskedQuestions)
    {
        //
    }
}
