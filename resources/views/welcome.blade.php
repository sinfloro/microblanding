@extends('layouts.app')

@section('contenido')
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators d-none">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img class="d-block w-100" src="https://www.beautystudio.com.co/wp-content/uploads/2022/07/banner-cejas.png"
                    alt="First slide">
                <div class="carousel-caption d-none d-md-block">
                    <h5>Titulo</h5>
                    <p>SubTitutlo</p>
                </div>
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="https://www.beautystudio.com.co/wp-content/uploads/2022/07/banner-cejas.png"
                    alt="Second slide">
                <div class="carousel-caption d-none d-md-block">
                    <h5>Titulo</h5>
                    <p>SubTitutlo</p>
                </div>
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="https://www.beautystudio.com.co/wp-content/uploads/2022/07/banner-cejas.png"
                    alt="Third slide">
                <div class="carousel-caption d-none d-md-block">
                    <h5>Titulo</h5>
                    <p>SubTitutlo</p>
                </div>
            </div>
        </div>
        <a class="carousel-control-prev prev-sm" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next prev-sm" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>

    {{--  slider de servicios  --}}
    <section class="pt-md-5 pb-md-5 servicios-md">
        <div class="container">
            <div class="row">
                <div class="col-6">
                    <h3 class="mb-3">Servicios</h3>
                </div>
                <div class="col-6 text-right">

                </div>
            </div>
            <div class="row">
                <div class="col-md-3 mb-3">
                    <div class="card w-100">
                        <img class="card-img-top"
                            src="https://www.beautystudio.com.co/wp-content/uploads/2022/07/banner-cejas.png"
                            alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">Card title</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of
                                the cards content.</p>
                            <a href="#" class="btn btn-dorado w-100">Go somewhere</a>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 mb-3">
                    <div class="card w-100">
                        <img class="card-img-top"
                            src="https://www.beautystudio.com.co/wp-content/uploads/2022/07/banner-cejas.png"
                            alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">Card title</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of
                                the cards content.</p>
                            <a href="#" class="btn btn-dorado w-100">Go somewhere</a>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 mb-3">
                    <div class="card w-100">
                        <img class="card-img-top"
                            src="https://www.beautystudio.com.co/wp-content/uploads/2022/07/banner-cejas.png"
                            alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">Card title</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of
                                the cards content.</p>
                            <a href="#" class="btn btn-dorado w-100">Go somewhere</a>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 mb-3">
                    <div class="card w-100">
                        <img class="card-img-top"
                            src="https://www.beautystudio.com.co/wp-content/uploads/2022/07/banner-cejas.png"
                            alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">Card title</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of
                                the cards content.</p>
                            <a href="#" class="btn btn-dorado w-100">Go somewhere</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="pb-5 quienes-sm">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="text-centrado titulo-sm">
                        <div class="line1">Que es</div>
                        <div class="line2" style="margin-left: 7rem;">Microblading?</div>
                    </div>
                </div>
                <div class="col-md-6 text-justify descripcion-sm">
                    <p>El microblading es una forma semipermanente de tatuaje de cejas que proporciona un resultado de
                        apariencia natural. Es una excelente opción para aquellos que buscan agregar definición y forma a
                        sus cejas sin esfuerzo. El proceso requiere un mantenimiento mínimo, lo que lo hace ideal para
                        personas ocupadas que no tienen el tiempo o el deseo de dibujarse las cejas todos los días.
                        Microblading también puede ayudar a las personas con cejas escasas, demasiado depiladas o dañadas.
                    </p>
                </div>

                <div class="col-md-6 text-justify descripcion-sm">
                    <p>En conclusión, el microblading es un procedimiento semipermanente que puede durar de 1 a 3 años.
                        Factores como el tipo de piel, el estilo de vida y el cuidado posterior afectarán la longevidad de
                        los resultados. Con el mantenimiento y los retoques adecuados, los clientes pueden lograr un hermoso
                        aspecto de cejas durante varios años. Para garantizar resultados óptimos, es importante elegir un
                        técnico capacitado y experimentado, así como los mejores productos disponibles. Tomarse el tiempo
                        para investigar antes de comprometerse con el procedimiento puede ayudar a garantizar resultados
                        duraderos.
                    </p>
                </div>
                <div class="col-md-6 p-xs- text-justify descripcion-sm">
                    <div class="text-centrado titulo-sm">
                        <div class="line1">Microblading</div>
                        <div class="line2" style="margin-left: -5%;">Cuanto tiempo dura?</div>
                    </div>
                </div>


                <div class="col-md-6">
                    <div class="text-centrado titulo-sm">
                        <div class="line1">Microblading</div>
                        <div class="line2" style="margin-left: 0rem;">Cual es su precio?</div>
                    </div>
                </div>
                <div class="col-md-6 text-justify descripcion-sm">
                    <p>El microblading es una forma semipermanente de tatuaje de cejas que proporciona un resultado de
                        apariencia natural. Es una excelente opción para aquellos que buscan agregar definición y forma a
                        sus cejas sin esfuerzo. El proceso requiere un mantenimiento mínimo, lo que lo hace ideal para
                        personas ocupadas que no tienen el tiempo o el deseo de dibujarse las cejas todos los días.
                        Microblading también puede ayudar a las personas con cejas escasas, demasiado depiladas o dañadas.
                    </p>
                </div>
            </div>
        </div>
    </section>

    <section class="llamado-accion llamado-accion-sm mt-5 mb-5">
        <div class="line2 line2-sm text-white">Decimos menos, Expresamos mas, contactanos!</div>
        @include('layouts.nuestro_whatsapp')
    </section>

    <section class="pb-5">
        <div class="container justify-content-center text-center text-dorado-mod">
            Trabajamos con las mejores marcas internacionales!
            <div class="row">
                <div class="col-md-8 offset-md-2  justify-content-center">
                    <div class="row">
                        <div class="col-md-4 imagen-ms">
                            <img src="https://static.wixstatic.com/media/292952_1bee9ce040c9407ea835e36c4761b088~mv2.png/v1/fill/w_192,h_93,al_c,q_85,usm_0.66_1.00_0.01,enc_auto/292952_1bee9ce040c9407ea835e36c4761b088~mv2.png"
                                class="w-100" alt="">
                        </div>
                        <div class="col-md-4 imagen-ms">
                            <img src="https://static.wixstatic.com/media/292952_4c67cce7770d479c81c9ca344f9af869~mv2.png/v1/fill/w_170,h_110,al_c,q_85,usm_0.66_1.00_0.01,enc_auto/292952_4c67cce7770d479c81c9ca344f9af869~mv2.png"
                                class="w-100" alt="">
                        </div>
                        <div class="col-md-4 imagen-ms">
                            <img src="https://static.wixstatic.com/media/292952_162d2cc29cda49edbf96213336673bb1~mv2_d_2966_1407_s_2.png/v1/fill/w_196,h_100,al_c,q_85,usm_0.66_1.00_0.01,enc_auto/Germaine.png"
                                alt="" class="w-100">
                        </div>
                        <div class="col-md-4 offset-md-2 imagen-ms">
                            <img src="https://static.wixstatic.com/media/292952_4c73828ae63244bba53ee0e3fb421433~mv2.png/v1/fill/w_194,h_77,al_c,q_85,usm_0.66_1.00_0.01,enc_auto/Selver%20thermal.png"
                                alt="" class="w-100">
                        </div>
                        <div class="col-md-4 imagen-ms">
                            <img src="https://static.wixstatic.com/media/292952_e866a49cbf4c431a8ca5ad35b9eba1e9~mv2.png/v1/fill/w_176,h_77,al_c,q_85,usm_0.66_1.00_0.01,enc_auto/292952_e866a49cbf4c431a8ca5ad35b9eba1e9~mv2.png"
                                alt="" class="w-100">
                        </div>
                    </div>



                </div>
            </div>
        </div>
    </section>
@endsection
