@extends('layouts.app')

@section('contenido')
<section class="header">
    <div class="line2 line2-sm text-white">Contactanos, estamos para atenderte!</div>
    <button class="btn btn-dark"> <i class="fab fa-whatsapp"></i> Nuestro Whatsapp</button>
</section>

<section>
    <div class="container mt-4 mtt-sm">
        <div class="row">
            <div class="col-md-8 text-center mtt-sm mb-3">
                <div class="line2 line2-sm">
                    Formulario de Contacto
                </div>

                <div class="col-md-12 text-left">
                    <div class="form-group">
                        <label for="">Nombre</label>
                        <input type="text" class="form-control" name="nombre">
                    </div>

                    <div class="form-group">
                        <label for="">Telefono</label>
                        <input type="text" class="form-control" name="telefono">
                    </div>

                    <div class="form-group">
                        <label for="">Email</label>
                        <input type="text" class="form-control" name="correo">
                    </div>

                    <div class="form-group">
                        <label for="">Mensaje</label>
                        <textarea name="mensaje" id="" class="form-control" rows="10"></textarea>
                    </div>

                    <button type="submit" class="btn btn-dorado w-100">Enviar</button>
                </div>
            </div>
            <div class="col-md-4 text-left mtt-sm">
                <div class="line2 line2-sm">
                    Adicionales
                </div>

                <p>P: (123) 456 78 95</p>
                <p>Horario: Lunes - Sabado 9:00AM - 5:00PM</p>
                <p>Correo: ventas@cejasperu.com</p>
            </div>
        </div>
    </div>
</section>
@endsection