<html>

<head>

    <title>Cejas Y Pestañas Lima </title>

    {{--  Hojas Estilos  --}}
    <link rel="stylesheet" href="{{ asset('bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('modern-business.css') }}">
    <link rel="stylesheet" href="{{ asset('styles.css?v-1.0.1') }}">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Tangerine">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.3.0/css/all.min.css"
        integrity="sha512-SzlrxWUlpfuzQ+pcUCosxcglQRNAq/DZjVsC0lE40xsADsfeQoEypE+enwcOiGjk/bSuGGKHEyjSoQ1zVisanQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
</head>

<body>
    @include('layouts.nav')


    {{--  slider principal  --}}
    @yield('contenido')

    <footer class="text-center text-lg-start bg-dorado text-muted">
        <!-- Section: Social media -->
        <section class="d-flex justify-content-center justify-content-lg-between p-4 border-bottom ">
            <!-- Left -->
            <div class="me-5 d-none d-lg-block text-white">
                <span>Contactanos en nuestras redes sociales:</span>
            </div>
            <!-- Left -->

            <!-- Right -->
            <div>
                <a href="" class="me-4 text-white">
                    <i class="fab fa-facebook"></i>
                </a>
                <a href="" class="me-4 text-white">
                    <i class="fab fa-twitter"></i>
                </a>
                <a href="" class="me-4 text-white">
                    <i class="fab fa-google"></i>
                </a>
                <a href="" class="me-4 text-white">
                    <i class="fab fa-instagram"></i>
                </a>
                <a href="" class="me-4 text-white">
                    <i class="fab fa-linkedin"></i>
                </a>
                <a href="" class="me-4 text-white">
                    <i class="fab fa-github"></i>
                </a>
            </div>
            <!-- Right -->
        </section>
        <!-- Section: Social media -->

        <!-- Section: Links  -->
        <section class="">
            <div class="container text-center text-md-start mt-5 ">
                <!-- Grid row -->
                <div class="row mt-3">
                    <!-- Grid column -->
                    <div class="col-md-4 col-lg-4 col-xl-4 mx-auto mb-4 text-white ">
                        <!-- Content -->
                        <h6 class="text-uppercase fw-bold mb-4 text-centrado">
                            <i class="fas fa-gem me-3"></i>Logo
                        </h6>

                    </div>
                    <!-- Grid column -->

                    <!-- Grid column -->
                    <div class="col-md-4 col-lg-4 col-xl-4 mx-auto mb-4 text-white">
                        <!-- Links -->
                        <h6 class="text-uppercase fw-bold mb-4">
                            Links Directos
                        </h6>
                        <p>
                            <a href="#!" class="text-reset">Servicios</a>
                        </p>
                        <p>
                            <a href="#!" class="text-reset">Blog</a>
                        </p>
                        <p>
                            <a href="#!" class="text-reset">Cursos</a>
                        </p>
                        <p>
                            <a href="#!" class="text-reset">Contacto</a>
                        </p>
                    </div>
                    <!-- Grid column -->


                    <!-- Grid column -->
                    <div class="col-md-4 col-lg-4 col-xl-4 mx-auto mb-md-0 mb-4 text-white">
                        <!-- Links -->
                        <h6 class="text-uppercase fw-bold mb-4">Contact</h6>
                        <p><i class="fas fa-home me-3"></i> New York, NY 10012, US</p>
                        <p>
                            <i class="fas fa-envelope me-3"></i>
                            info@example.com
                        </p>
                        <p><i class="fas fa-phone me-3"></i> + 01 234 567 88</p>
                        <p><i class="fas fa-print me-3"></i> + 01 234 567 89</p>
                    </div>
                    <!-- Grid column -->
                </div>
                <!-- Grid row -->
            </div>
        </section>


        @include('layouts.whatsapp')

        <script src="{{ asset('jquery.min.js') }}"></script>
        <script src="{{ asset('bootstrap.bundle.min.js') }}"></script>
</body>

</html>
