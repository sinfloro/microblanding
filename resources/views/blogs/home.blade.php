@extends('layouts.app')

@section('contenido')
    <section class="header">
        <div class="line2 line2-sm text-white">Enterate de las ultimas novedades!</div>
        <button class="btn btn-dark"> <i class="fab fa-whatsapp"></i> Nuestro Whatsapp</button>
    </section>

    <div class="container">
        <div class="col-md-10 offset-md-1 mt-3 mb-3">
            @for ($i = 1; $i <= 10; $i++)
            <div class="card w-100 mt-3" style="width: 18rem;">
                <img class="card-img-top" src="https://www.beautystudio.com.co/wp-content/uploads/2022/07/banner-cejas.png"
                    alt="Card image cap">
                <div class="card-body">
                    <h4>Titulo del post</h4>
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the
                        cards content.</p>
                    <a href="{{ route('blogs.post') }}" class="btn btn-dorado w-100">Leer Mas</a>
                </div>
            </div>
            @endfor
        </div>
    </div>
@endsection
