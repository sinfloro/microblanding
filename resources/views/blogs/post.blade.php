@extends('layouts.app')

@section('contenido')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <img src="https://www.beautystudio.com.co/wp-content/uploads/2022/07/banner-cejas.png"
                    class="img-fluid w-100 rounded-pill mt-3" alt="">
            </div>

            <div class="col-md-12 text-center mt-4 mb-5">
                <h2>Titulo</h2>
            </div>

            <div class="col-md-12 mb-5 text-justify">


                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque convallis, dolor nec posuere luctus,
                augue lorem condimentum ante, sed accumsan justo lorem sit amet neque. Curabitur laoreet nisl vitae sagittis
                suscipit. Donec et tincidunt urna, vitae blandit mi. Nam hendrerit, urna in rhoncus tempus, enim est
                volutpat sapien, eu aliquet urna elit ac risus. Aenean at erat tempus, aliquet mi eget, luctus quam.
                Phasellus justo justo, rutrum venenatis tincidunt vitae, scelerisque eget elit. Praesent elementum arcu a
                fringilla efficitur. Ut nunc lectus, pulvinar vitae sollicitudin in, ornare non nunc. Fusce nec ipsum vitae
                nisi scelerisque commodo. Nullam tortor justo, laoreet sit amet risus id, ornare consectetur lacus. Duis sed
                aliquet purus, sit amet accumsan orci. Phasellus venenatis, metus et volutpat elementum, lacus quam viverra
                diam, non mattis tortor lacus ut est. Pellentesque viverra sollicitudin consectetur. Suspendisse gravida
                odio sem, non iaculis ante semper vel. Aenean ac ultrices libero.</p>

                <p>Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Ut ligula urna,
                convallis vel mattis at, dignissim quis nisi. Curabitur pellentesque tellus lobortis varius vehicula.
                Suspendisse ac arcu laoreet, tristique elit at, efficitur orci. Orci varius natoque penatibus et magnis dis
                parturient montes, nascetur ridiculus mus. Donec efficitur leo ac dui ultrices, a consectetur massa
                venenatis. Donec enim ex, egestas et rutrum sed, accumsan eget mauris. Nunc gravida vulputate augue, nec
                porttitor arcu tincidunt in.</p>

                <p>Sed consequat iaculis diam mattis molestie. Praesent at vehicula urna, eu congue tortor. Praesent id mattis
                enim, vel imperdiet elit. Vivamus aliquam suscipit elementum. Quisque dictum eros ut lacus malesuada
                efficitur a vitae eros. Cras id ante finibus, blandit ligula a, mollis tellus. Sed dui purus, lobortis sit
                amet consequat eu, laoreet vel ante. Ut quis arcu non nisl vulputate porttitor. Donec viverra mollis
                eleifend.</p>

                <p>In tincidunt nunc urna, eu luctus nulla molestie in. Proin bibendum risus eget nibh sollicitudin feugiat.
                Vivamus tincidunt tempus egestas. Cras in vestibulum nulla. Nullam in ullamcorper ex. Proin eget sem sed
                neque gravida vehicula. Integer vel metus vitae eros cursus ultricies. Phasellus malesuada varius rhoncus.
                Fusce molestie turpis ut lorem fermentum lobortis. Nulla eu massa vitae massa accumsan dignissim. Ut
                sollicitudin est augue, eu convallis eros molestie a. Praesent erat sem, euismod a aliquet et, finibus sed
                diam.</p>

                <p>Nulla rhoncus tempus ante, vel varius tellus tristique non. Pellentesque ut purus quis ipsum consectetur
                imperdiet quis sit amet velit. Aliquam facilisis nunc quis erat egestas, nec venenatis justo auctor. Sed
                rutrum enim eu orci ullamcorper bibendum. Donec quis vulputate massa. Aliquam imperdiet felis at mauris
                cursus rhoncus. Donec ac purus eu mi consectetur tincidunt sit amet sit amet nulla.</p>

                <p>Maecenas sollicitudin, tellus id scelerisque lobortis, est felis lobortis justo, eu volutpat diam turpis eu
                ligula. Quisque sed porttitor nisi. Sed tempor lacus nec purus commodo, id convallis nisl laoreet. Donec
                semper, turpis at sodales accumsan, justo turpis rutrum tortor, quis faucibus neque justo in ipsum. Nam
                malesuada hendrerit orci, eget porttitor lorem dignissim eget. Praesent rutrum lectus ligula, a scelerisque
                ipsum posuere a. Maecenas malesuada, felis eget dignissim consectetur, tortor est efficitur felis, quis
                molestie nulla enim nec sapien.</p>

                <p>Curabitur sed risus at urna scelerisque blandit sit amet vitae magna. Fusce ut varius sem. Nulla molestie
                consectetur purus, ullamcorper consequat nisi bibendum eget. Aliquam iaculis risus eleifend mauris feugiat
                mattis. Mauris tincidunt vitae lectus id condimentum. Nullam tristique semper lacus. Donec non tellus
                tortor.  aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p>

                <p>Aliquam gravida eros non odio feugiat aliquet. Orci varius natoque penatibus et magnis dis parturient
                montes, nascetur ridiculus mus. Praesent elit purus, gravida sit amet est ac, molestie maximus libero.
                Vestibulum sed viverra nibh, quis elementum libero. Aenean a lacinia risus. Etiam non sapien quis nulla
                sagittis lobortis consequat ultrices erat. Suspendisse in eros nec tortor posuere elementum. Sed dolor est,
                sodales ac convallis eu, malesuada vitae nibh. Curabitur tempor rutrum elit vel posuere. Cras eget eros
                maximus, gravida nibh in, tempor libero. Maecenas venenatis sapien ac est gravida gravida. Duis nec nunc
                eget arcu ultrices venenatis viverra ac diam. Fusce pharetra vehicula sapien, sed molestie velit vehicula a.
                Proin quis ipsum tincidunt, luctus neque nec, facilisis elit.</p>


            </div>
        </div>
    </div>
@endsection
